﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
using UnityEngine;

public partial class Game
{
    #region Fields

    private const int SAMPLES_ARRAY_LENGTH = 512;
    private const int BANDS_ARRAY_LENGTH = 8;

    [Space]
    [Header("Microphone Input")]
    [SerializeField]
    private GameObject m_microphoneNotification;
    [SerializeField]
    private AudioSource m_audioSource = null;
    [SerializeField]
    private float m_sensitivity = 1000;
    private bool m_hasMicrophone;

    private float[] m_microphoneSamplesArray = new float[SAMPLES_ARRAY_LENGTH];

    #endregion
    #region Methods

    private void InitMicrophoneInput()
    {
#if UNITY_WEBGL
        Application.RequestUserAuthorization(UserAuthorization.Microphone);
#endif

        m_hasMicrophone = Microphone.devices.Length > 0;
        if (m_hasMicrophone)
        {
            m_audioSource.clip = Microphone.Start(Microphone.devices[0], true, 1, AudioSettings.outputSampleRate);
            m_audioSource.Play();
        }

        m_microphoneNotification.SetActive(!m_hasMicrophone);
    }

    private int GetMicrophoneInput()
    {
        m_audioSource.GetSpectrumData(m_microphoneSamplesArray, 0, FFTWindow.Blackman);

        var bandStart = 0;
        var maxValue = float.MinValue;
        for (var i = 0; i < BANDS_ARRAY_LENGTH; ++i)
        {
            var delta = (int)Mathf.Pow(2, i) * 2;

            var bandEnd = bandStart + delta;
            var avarage = 0f;
            for (var j = bandStart; j < bandEnd; ++j)
                avarage += m_microphoneSamplesArray[j];

            avarage = avarage / delta;
            if (avarage > maxValue)
                maxValue = avarage;

            bandStart = bandEnd;
        }

        return (int)(maxValue * m_sensitivity);
    }

    #endregion
}