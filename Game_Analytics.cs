﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
using System.Collections.Generic;
using UnityEngine.Analytics;

public partial class Game
{
    #region Fields

    private const string GAME_RESTARTED_EVENT = "GameRestarted";

    private const string GAME_OVER_EVENT_NAME = "GameOver";
    private const string SCORE_KEY = "score";

    #endregion
    #region Methods

    private void SendRestartEvent()
    {
        Analytics.CustomEvent(GAME_RESTARTED_EVENT);
    }

    private void SendGameOverEvent(int score)
    {
        Analytics.CustomEvent(GAME_OVER_EVENT_NAME, new Dictionary<string, object>() { { SCORE_KEY, score } });
    }

    #endregion
}