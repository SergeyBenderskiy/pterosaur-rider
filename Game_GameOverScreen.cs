﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
using UnityEngine;
using UnityEngine.UI;

public partial class Game
{
    #region Fields

    private const string SCORE_DISPLAY_FORMAT = "Your Score : {0}";

    [Space]
    [Header("GameOverScreen")]
    [SerializeField]
    private Text m_scoreText;

    [SerializeField]
    private GameObject m_gameOverScreen;

    #endregion
    #region Methods

    private void HideGameOverScreen()
    {
        m_gameOverScreen.SetActive(false);
    }

    private void ShowGameOverScreen(int score)
    {
        m_scoreText.text = string.Format(SCORE_DISPLAY_FORMAT, score);
        m_gameOverScreen.SetActive(true);
    }

    #endregion
}