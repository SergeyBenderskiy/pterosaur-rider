﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
using UnityEngine;

public partial class Game : MonoBehaviour
{
    #region Fields

    private const float COLLISION_THRESHOLD = 0.65f;

    private bool m_isOver = false;
    private float m_startTime;

    #endregion
    #region Methods

    public void RestartGame()
    {
        SendRestartEvent();
        ClearEnemiesList();
        ClearScenery();

        PrepareNewGame();
        m_isOver = false;
    }

    private void PrepareNewGame()
    {
        m_startTime = Time.time;
        InitScenery();
        InitHero();

        HideGameOverScreen();
        StartSpawningEnemies();
    }

    private void Start()
	{
        InitMicrophoneInput();
        InitCamera();

        PrepareNewGame();
    }

	private void Update()
	{
        if (m_isOver)
            return;

        var inputValue = m_hasMicrophone ? GetMicrophoneInput() : 0;
        HandleHeroInput(inputValue);
        CheckForGameOver();

#if !UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
#endif
    }

    private void CheckForGameOver()
    {
        if (DidHitTheGround() || DidHitEnemy())
        {
            HandleGameOver();
        }
    }

    private bool DidHitTheGround()
    {
        var heroRect = GetRect(m_heroTransform);
        return heroRect.y < 0.5f * (heroRect.height - Screen.height);
    }

    private bool DidHitEnemy()
    {
        var heroRect = GetRect(m_heroTransform);
        heroRect.height *= COLLISION_THRESHOLD;
        heroRect.width *= COLLISION_THRESHOLD;

        foreach (var enemy in m_spawnedEnemiesList)
        {
            var enemyRect = GetRect(enemy);
            enemyRect.height *= COLLISION_THRESHOLD;
            enemyRect.width *= COLLISION_THRESHOLD;
            if (heroRect.Overlaps(enemyRect))
                return true;
        }

        return false;
    }

    private void HandleGameOver()
    {
        m_isOver = true;
        StopAllCoroutines();

        var score = Mathf.FloorToInt(Time.time - m_startTime);
        SendGameOverEvent(score);
        ShowGameOverScreen(score);
    }

    private void InitCamera()
    {
        Camera.main.orthographicSize = Screen.height / 2f;
    }

#endregion
}