﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Game
{
    #region Fields

    [Space]
    [Header("Enemies")]
    [SerializeField]
    private Transform m_enemyPrefab;
    [SerializeField]
    private float m_enemyBaseSpeed = 150f;
    [SerializeField]
    private float m_speedIncrement = 0.25f;
    [SerializeField]
    private float m_increaseSpeedIntervals = 10f;

    [SerializeField]
    private float m_spawnEnemyInterval = 3f;

    private List<Transform> m_spawnedEnemiesList = new List<Transform>();

    #endregion
    #region Methods

    private void ClearEnemiesList()
    {
        m_spawnedEnemiesList.ForEach(enemy => Destroy(enemy.gameObject));
        m_spawnedEnemiesList.Clear();
    }

    private void StartSpawningEnemies()
    {
        StartCoroutine(DoSpawnEnemies());
    }

    private IEnumerator DoSpawnEnemies()
    {
        var positionsToSpawnLit = new List<float>();

        while (true)
        {
            var currentY = m_heroTransform.position.y;
            positionsToSpawnLit.Clear();
            positionsToSpawnLit.Add(currentY);

            var enemyRect = GetRect(m_enemyPrefab);
            var enemyHeight = enemyRect.height;

            var maxAltitude =  0.5f * (Screen.height - enemyHeight);
            while (currentY <= maxAltitude)
            {
                if (!positionsToSpawnLit.Contains(currentY))
                    positionsToSpawnLit.Add(currentY);

                currentY += enemyHeight;
            }

            var minAltitude = -maxAltitude;
            currentY = positionsToSpawnLit[0];
            while (currentY >= minAltitude)
            {
                if (!positionsToSpawnLit.Contains(currentY))
                    positionsToSpawnLit.Add(currentY);

                currentY -= enemyHeight;
            }

            positionsToSpawnLit.Sort();
            var enemyCount = Random.Range(1, 3);
            for (var i = 0; i < enemyCount && positionsToSpawnLit.Count > 0; ++i)
            {
                var index = Random.Range(0, positionsToSpawnLit.Count);
                var spawnPosition = positionsToSpawnLit[index];
                SpawnNewEnemy(m_enemyPrefab, spawnPosition);
                positionsToSpawnLit.RemoveAt(index);
                if (index < positionsToSpawnLit.Count && Mathf.Abs(positionsToSpawnLit[index] - spawnPosition) <= enemyHeight + 0.1f)
                {
                    positionsToSpawnLit.RemoveAt(index);
                }
                if (index > 0 && Mathf.Abs(positionsToSpawnLit[index - 1] - spawnPosition) <= enemyHeight + 0.1f)
                {
                    positionsToSpawnLit.RemoveAt(index - 1);
                }
            }

            yield return new WaitForSeconds(m_spawnEnemyInterval);
        }
    }

    private void SpawnNewEnemy(Transform prefab, float y)
    {
        var enemy = Instantiate(prefab);
        enemy.position = new Vector3(0.5f * (Screen.width + GetRect(enemy).width), y);

        m_spawnedEnemiesList.Add(enemy);
        StartCoroutine(DoMoveEnemy(enemy, GetEnemyCurrentSpeed()));
    }

    private float GetEnemyCurrentSpeed()
    {
        return m_enemyBaseSpeed * (1f + m_speedIncrement * Mathf.Floor((Time.time - m_startTime) / m_increaseSpeedIntervals));
    }

    private IEnumerator DoMoveEnemy(Transform enemy, float speed)
    {
        var leftmostPosition = -0.5f * Screen.width - GetRect(enemy).width;
        while (leftmostPosition < enemy.position.x)
        {
            yield return null;
            var deltaX = speed * Time.deltaTime;
            var enemyPosition = enemy.position;
            enemyPosition.x -= deltaX;

            enemy.position = enemyPosition;
        }

        m_spawnedEnemiesList.Remove(enemy);
        Destroy(enemy.gameObject);
    }

    #endregion
}