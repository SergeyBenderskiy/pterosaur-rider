﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
using UnityEngine;

public partial class Game
{
    #region Fields

    [Space]
    [Header("Hero")]
    [SerializeField]
    private Transform m_heroTransform;
    [SerializeField]
    private float m_heroDescendSpeed;
    [SerializeField]
    private float m_heroAscendSpeed;

    #endregion
    #region Methods

    private void InitHero()
    {
        m_heroTransform.position = new Vector2((GetRect(m_heroTransform).width - Screen.width) / 2, 0);
    }

    private void HandleHeroInput(int inputValue)
    {
        var deltaY = inputValue > 0 ? m_heroAscendSpeed * inputValue : -m_heroDescendSpeed;
        deltaY *= Time.deltaTime;
        var position = m_heroTransform.position;

        var halfScreenHeight = Screen.height / 2;
        var halfHeroHeight = GetRect(m_heroTransform).height / 2;
        var maxAltitude = halfScreenHeight - halfHeroHeight;
        var minAltitude = -halfScreenHeight;

        position.y = Mathf.Clamp(position.y + deltaY, minAltitude, maxAltitude);
        m_heroTransform.position = position;
    }

    private Rect GetRect(Transform transform)
    {
        var renderer = transform.GetComponent<SpriteRenderer>();
        var rect = renderer.sprite.rect;
        rect.x = transform.position.x;
        rect.y = transform.position.y;

        return rect;
    }

    #endregion
}