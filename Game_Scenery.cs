﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Game
{
    #region Fields

    [Space]
    [Header("Scenery")]
    [SerializeField]
    private Transform[] m_mountainsPrefabsArray;

    private List<Transform> m_currentSceneryList = new List<Transform>();

    #endregion
    #region Methods

    private void InitScenery()
    {
        var posY = -Screen.height / 2;

        var startX = -0.5f * Screen.width;
        var endX = -startX;
        while (startX <= endX)
        {
            var prefab = GetRandomSceneryPrefab();
            var element = Instantiate(prefab);
            m_currentSceneryList.Add(element);

            var elementPosition = new Vector3(startX, posY);

            element.position = elementPosition;
            var elenmentRect = GetRect(element);
            var elementWidth = elenmentRect.width;

            startX = Random.Range(startX + 0.5f * elementWidth, startX + 1.1f * elementWidth);
        }
    }

    private void ClearScenery()
    {
        m_currentSceneryList.ForEach(element => Destroy(element.gameObject));
        m_currentSceneryList.Clear();
    }

    private Transform GetRandomSceneryPrefab()
    {
        var index = Random.Range(0, m_mountainsPrefabsArray.Length);
        return m_mountainsPrefabsArray[index];
    }

    #endregion
}